package com.example.teamapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.ImageView;

public class AnotherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        ImageView image = findViewById(R.id.imageView);
        image.setImageResource(getResources().getIdentifier("mouse", "drawable", getPackageName()));
    }

}
